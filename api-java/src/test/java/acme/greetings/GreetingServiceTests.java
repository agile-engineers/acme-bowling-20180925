package acme.greetings;

import org.junit.Assert;
import org.junit.Test;

public class GreetingServiceTests {

    @Test
    public void build_default_greeting() {
        GreetingService sut = new GreetingService();

        Greeting result = sut.build();

        Assert.assertEquals("Hello, World!", result.getText());
    }

}
