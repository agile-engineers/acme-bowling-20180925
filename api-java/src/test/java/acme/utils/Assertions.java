package acme.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;

public class Assertions {
    public static void deepEquals(Object o1, Object o2) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        Assert.assertEquals(
                mapper.writeValueAsString(o1),
                mapper.writeValueAsString(o2));
    }
}
