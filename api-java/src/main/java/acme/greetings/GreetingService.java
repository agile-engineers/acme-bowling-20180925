package acme.greetings;

class GreetingService {
    GreetingService() {
    }

    Greeting build() {
        return new Greeting("Hello, World!");
    }
}
