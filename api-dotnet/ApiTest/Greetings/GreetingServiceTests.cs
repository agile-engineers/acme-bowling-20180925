using ApiWeb.Greetings;
using Xunit;

namespace ApiTest.Greetings
{
    public class GreetingServiceTests
    {
        [Fact]
        public void build_a_default_greeting()
        {
            GreetingService sut = new GreetingService();

            Greeting actual = sut.Build();

            Assert.Equal("Hello, World!", actual.Text);
        }
    }

}