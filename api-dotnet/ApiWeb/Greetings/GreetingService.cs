namespace ApiWeb.Greetings
{
    public class GreetingService
    {
        public Greeting Build()
        {
            return new Greeting {Text = "Hello, World!"};
        }
    }
}