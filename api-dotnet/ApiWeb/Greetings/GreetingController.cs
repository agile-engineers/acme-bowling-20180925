using Microsoft.AspNetCore.Mvc;

namespace ApiWeb.Greetings
{
    [Route("greeting")]
    public class GreetingController : Controller
    {
        [HttpGet]
        public IActionResult Get()
        {
            var service = new GreetingService();
            
            return Ok(service.Build());
        }
    }
}