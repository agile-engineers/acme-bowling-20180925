import "source-map-support/register"

import Express from "express"
import CORS from "cors"
import BodyParser from "body-parser"

import { GreetingService } from "./greetings"

const port = process.env.PORT || 8081
const app = Express()

app.use( CORS( {
    origin: "http://localhost:8080",
    credentials: true,
} ) )

app.use( BodyParser.json() )

app.get( "/greeting", function ( req, res ) {
    const greetingService = new GreetingService()

    res.send( greetingService.build() )
} )

app.listen( port, function () {
    console.log( "api server is started: " + port )
} )
