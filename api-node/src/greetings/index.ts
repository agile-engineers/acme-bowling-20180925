export class Greeting {
    text: string
}

export class GreetingService {
    build ( name: string = "World") {
        return { text: `Hello, ${ name }!` }
    }
}