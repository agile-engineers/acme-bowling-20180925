import { assert } from "chai"
import { suite, test } from "mocha"

import { Greeting, GreetingService } from "./index"

suite( "greeting service", function () {

    test( "build default greeting", function () {
        const sut = new GreetingService()

        const result: Greeting = sut.build()

        assert.equal( result.text, "Hello, World!" )
    } )

} )
