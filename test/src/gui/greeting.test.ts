import { assert } from "chai"
import { suite, test } from "mocha"

import { assertListAs, assertTextAs, initializeWebDriver, takeScreenShot } from "./utils"

suite( "home page", function () {

    beforeEach( function () {
        this.driver = initializeWebDriver()

        return this.driver
            .url( "http://localhost:8080/" )
    } )

    afterEach( function () {
        return this.driver.end()
    } )

    test( "a default greeting is displayed", function () {
        return this.driver
            .waitForVisible( "#message" )
            .then( assertTextAs( "#message", "Whazzup, Dog!" ) )
    } )

    test( "a custom greeting given a name", function () {
        return this.driver
            .waitForVisible( "#message" )
            .then( takeScreenShot )
            .setValue( "form input", "1111" )
            .click( "form button" )
            .then( assertListAs( "#players", "Hello, joe!" ) )
    } )
} )