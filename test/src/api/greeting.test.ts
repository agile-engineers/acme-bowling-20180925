import { assert } from "chai"
import { suite, test } from "mocha"

import { Assertions, URLClient } from "./utils"

suite( "greeting", function () {
    const url = URLClient.withBaseURL( "http://localhost:8081" )

    test( "fetch a default greeting", function () {
        return url.get( "/greeting" )
            .then( Assertions.responseEquals( { text: "Hello, World!" } ) )
    } )

    test( "fetch greeting with name", function () {
        return url.get( "/greeting/joe" )
            .then( Assertions.responseEquals( { text: "Hello, joe!" } ) )
    } )

    test( "invalid url returns error", function () {
        return url.get( "/greetings" )
            .then( Assertions.failsWith( 404 ) )
    } )
} )